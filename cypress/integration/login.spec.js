context("Login Page", () => {
    it('log in with valid credentials', () => {
        cy.viewport(1440, 900)
        cy.visit('https://teste-front-end-j58fy8my0-leandroarruda.vercel.app/')
        cy.contains('Email')
        cy.contains('Senha')
        cy.get(':nth-child(1) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input').type('leandro@mail.com')
        cy.get(':nth-child(2) > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input').type('bestPassw0rd')
        cy.get('.MuiButtonBase-root').click()
        cy.get('.sc-bdnxRM').click()
        cy.contains('Administrador')
    })
})
import styled, { keyframes } from "styled-components";


const transOpacity = keyframes`
    from{
        opacity:0;
    }
    to {
        opacity: 1;
    }
`

export const Container = styled.div`
    margin-top: 20px;
    width: 98vw;
    height: 100vh;
    animation: ${transOpacity} 1s linear;
`
import { useState } from "react"
import { GoogleMap, withScriptjs, withGoogleMap, Marker, InfoWindow } from "react-google-maps"
import { useUser } from "../../providers/User"
import { Container } from "./style"

function Map() {
    const { municipiosSC, patientsList } = useUser()

    const[selected,setSelected] = useState(null)

    const qtdVaccines = () => {
        let obj = {}
        patientsList.forEach(e => {
            if(obj[e["paciente_endereco_nmmunicipio"]]){
                obj[e["paciente_endereco_nmmunicipio"]]++
            }else{
                obj[e["paciente_endereco_nmmunicipio"]] = 1
            }
        });
        return obj
    }

    return (
        <GoogleMap
            defaultZoom={8}
            defaultCenter={{ lat: -27.0628367, lng: -51.114965 }}
            >
            {municipiosSC.map(mun=> qtdVaccines()[mun["nome"].toUpperCase()] ? (
                    <Marker 
                        key={mun["codigo_ibge"]} 
                        position= {{lat: mun["latitude"], lng: mun["longitude"]}} 
                        onClick={()=>setSelected(mun)}
                        icon= {{url:'/peoples.png'}}
                    />
                ) : null
            )}
            {!!selected && 
                <InfoWindow
                    position = {{lat: selected.latitude, lng: selected.longitude}}
                    onCloseClick={()=>setSelected(false)}
                >
                    <div>
                        <h1>{selected.nome}</h1>
                        <h2>{qtdVaccines()[selected["nome"].toUpperCase()]} pessoas vacinadas</h2>
                    </div>
                </InfoWindow>
            }
            </GoogleMap>
    )
}
    
const MyMapComponent = withScriptjs(withGoogleMap(Map))

const PatientsMap = () => {
    const { setLoading } = useUser()

    setTimeout(() => {
        setLoading(false)
    }, 1500);

    return (
        <Container >
            <MyMapComponent
                isMarkerShown
                googleMapURL={
                `https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyDvzmHDUQ5XjFfuyL_cJEdMDO83v0f--P0`
                }
                loadingElement={<div style={{ height: `100%` }} />}
                containerElement={<div style={{ height: `520px` }} />}
                mapElement={<div style={{ height: `100%` }} />}
            />
        </Container>
    )

}

export default PatientsMap


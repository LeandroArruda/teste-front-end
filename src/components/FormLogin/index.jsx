import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { Button, TextField } from '@material-ui/core'
import { useUser } from '../../providers/User'
import { Container } from "./style"
import "../../styles/Global.css"

const FormLogin = () => {

    const { login, setLoading } = useUser()

    const schema = yup.object().shape({
        email: yup.string().email('Email inválido').required('campo obrigatório!'),
        password: yup
            .string()
            .required("campo obrigatório!")
    })

    const { register, handleSubmit, formState: { errors }, reset} = useForm({
        resolver:yupResolver(schema)
    })

    const handleForm = data => {
        setLoading(true)
        login(data)
        reset()
    }

    return (
        <Container>
            <form onSubmit = {handleSubmit(handleForm)}>
                <div>
                    <TextField 
                        className='inputs'
                        margin = 'normal'
                        variant = 'outlined'
                        label = 'Email'
                        name = 'email'
                        size = 'small'
                        color = 'primary'
                        inputProps = {register("email")}
                        error = {!!errors.email}
                        helperText = {errors.email?.message}
                    />
                </div>
                <div>
                    <TextField 
                        className='inputs'
                        margin = 'normal'
                        variant = 'outlined'
                        label = 'Senha'
                        name = 'password'
                        size = 'small'
                        color = 'primary'
                        type = 'password'
                        inputProps = {register('password')}
                        error = {!!errors.password}
                        helperText = {errors.password?.message}
                    />
                </div>
                <div style={{display:"flex", marginTop:"20px", flexDirection:"column"}}>
                    <Button 
                        type='submit' 
                        variant='contained' 
                        color='primary'
                        className='inputs'    
                    >
                            Entrar
                    </Button>
                </div>
            </form>
        </Container>
    )
}

export default FormLogin
import styled, { keyframes } from "styled-components";

const transOpacity = keyframes`
    from{
        opacity:0;
    }
    to {
        opacity: 1;
    }
`
export const Container = styled.div`
    width: 100%;
    max-width: 300px;
    background-color: white;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    border-radius: 20px;
    padding: 20px;
    animation: ${transOpacity} 2s linear;
`


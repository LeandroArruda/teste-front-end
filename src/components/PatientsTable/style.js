import styled from "styled-components";


export const Container = styled.div`
    width: 100%;
    max-width: 1250px;
    height: auto;
    display: flex;
    justify-content: space-between;
    align-items: end;
    flex-direction: column;
    background-color: white;
    border-radius: 10px;
    margin-top: 20px;

    .responsive {
            width: 100%;
            display: flex;
            justify-content: space-between;
    }

    div{
        margin: 4px 0px;
    }
    form {
        display: flex;
    }
    @media(max-width:623px){
        .responsive {
            width: 100%;
            display: flex;
            justify-content: space-between;
            align-items: end;
            flex-direction: column;
        }
    }
`
import "./style.css"
import React from 'react';
import PropTypes from 'prop-types';
import { withStyles, makeStyles, useTheme } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableFooter from '@material-ui/core/TableFooter';
import TablePagination from '@material-ui/core/TablePagination';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import FirstPageIcon from '@material-ui/icons/FirstPage';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import LastPageIcon from '@material-ui/icons/LastPage';
import TableHead from '@material-ui/core/TableHead';
import { useUser } from '../../providers/User';
import { Button, FormControl, InputLabel, MenuItem, Select } from "@material-ui/core";
import { useState } from "react";
import { Container } from "./style"


const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;
  
  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };
  
  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };
  
  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };
  
  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };
  
  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton onClick={handleBackButtonClick} disabled={page === 0} aria-label="previous page">
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}


TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

function createData(idade, sexo, raca, municipio, estado, estabelecimento, grupo, categoria, vacina, data) {
  return {idade, sexo, raca, municipio, estado, estabelecimento, grupo, categoria, vacina, data };
}

const useStyles2 = makeStyles({
  table: {
    minWidth: 500,
  },
});

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

export default function CustomPaginationActionsTable() {

  const { patientsList } = useUser()
  const [field,setField] = useState()
  const [value,setValue] = useState()
  const [list,setList] = useState([])
  const [filter,setFilter] = useState(false)

  const rows = patientsList.map(e => createData(
        e.paciente_idade,e.paciente_enumsexobiologico,e.paciente_racacor_valor,
        e.paciente_endereco_nmmunicipio,e.paciente_endereco_uf,e.estalecimento_nofantasia,
        e.vacina_grupoatendimento_nome,e.vacina_categoria_nome,e.vacina_nome,e.vacina_dataaplicacao
      )  
  );

  const classes = useStyles2();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [isDisabled,setIsDisabled] = useState(true)


  const emptyRows = rowsPerPage - Math.min(rowsPerPage, filter ? filter.length : rows.length - page * rowsPerPage);
    

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleFields = (value) =>{
    if(value)
    setIsDisabled(false)
    else
    setIsDisabled(true)
    let l = []
    patientsList.forEach(e=>l.indexOf(e[value]) === -1 && l.push(e[value]))
    setList(l)
    setField(value)
  }

  const handleValue = (value) => {
    if(!value){
      setFilter(false)
    }
    else{
      setFilter(patientsList.filter(e=>e[field] === value).map(e=> createData(
        e.paciente_idade,e.paciente_enumsexobiologico,e.paciente_racacor_valor,
        e.paciente_endereco_nmmunicipio,e.paciente_endereco_uf,e.estalecimento_nofantasia,
        e.vacina_grupoatendimento_nome,e.vacina_categoria_nome,e.vacina_nome,e.vacina_dataaplicacao
      )))
    }
    setValue(value)
  }

  const resetFields = () => {
    setIsDisabled(true)
    setValue("")
    setField("")
    setFilter(false)
  }

  return (
    <Container>
      <form>
        <div className="responsive">
          <div>
            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel id="demo-simple-select-outlined-label">filtrar por...</InputLabel>
                <Select
                  className="inputs"
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  value={field}
                  onChange={(e)=>handleFields(e.target.value)}
                  label="Age"
                  style={{margin:"4px 3px"}}
                >
                  <MenuItem disabled value=""><em>Opções</em></MenuItem>
                  <MenuItem value='paciente_enumsexobiologico'>Sexo</MenuItem>
                  <MenuItem value='paciente_racacor_valor'>Raça</MenuItem>
                  <MenuItem value='paciente_endereco_uf'>UF do vacinado</MenuItem>
                  <MenuItem value='estabelecimento_uf'>UF do estabelecimento</MenuItem>
                  <MenuItem value='vacina_categoria_nome'>Categoria</MenuItem>
                </Select>
            </FormControl>
          </div>
          <div>
            <FormControl variant="outlined" className={classes.formControl}>
                <InputLabel id="demo-simple-select-outlined-label">Valor...</InputLabel>
                <Select
                  className="inputs"
                  displayEmpty
                  labelId="demo-simple-select-outlined-label"
                  id="demo-simple-select-outlined"
                  value={value}
                  onChange={(e)=>handleValue(e.target.value)}
                  label="valor"
                  disabled={isDisabled}
                  style={{margin:"4px 3px"}}
                >
                  <MenuItem value=""></MenuItem>
                  {list.map((e,key)=><MenuItem value={e} key={key}>{e}</MenuItem>)}
                </Select>
            </FormControl>
          </div>
          <div>
            <Button 
              className="reset" 
              type='reset' 
              onClick={resetFields} 
              variant='contained' 
              color='primary'
              style={{transform:"translateY(8px)"}}
            >
                Reset
            </Button>
          </div>
        </div>
      </form>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="custom pagination table">
        <TableHead>
            <TableRow>
              <StyledTableCell >Idade</StyledTableCell>
              <StyledTableCell >Sexo</StyledTableCell>
              <StyledTableCell >Raça/cor</StyledTableCell>
              <StyledTableCell >Município</StyledTableCell>
              <StyledTableCell >Estado</StyledTableCell>
              <StyledTableCell >Estabelecimento</StyledTableCell>
              <StyledTableCell >Grupo</StyledTableCell>
              <StyledTableCell >Categoria</StyledTableCell>
              <StyledTableCell >Vacina</StyledTableCell>
              <StyledTableCell >Data</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {(rowsPerPage > 0 && filter ?
              filter.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage) 
              : rowsPerPage > 0 ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              : filter ? filter : rows
            ).map((row, index) => (
              <TableRow key={index}>
                <TableCell style={{ width: 80 }}  component="th" scope="row">
                  {row.idade}
                </TableCell>
                <TableCell style={{ width: 80 }} >
                  {row.sexo}
                </TableCell>
                <TableCell style={{ width: 160 }} >
                  {row.raca}
                </TableCell>
                <TableCell style={{ width: 160 }} >
                  {row.municipio}
                </TableCell>
                <TableCell style={{ width: 160 }} >
                  {row.estado}
                </TableCell>
                <TableCell style={{ width: 160 }} >
                  {row.estabelecimento}
                </TableCell>
                <TableCell style={{ width: 160 }} >
                  {row.grupo}
                </TableCell>
                <TableCell style={{ width: 160 }} >
                  {row.categoria}
                </TableCell>
                <TableCell style={{ width: 160 }} >
                  {row.vacina}
                </TableCell>
                <TableCell style={{ width: 160 }} >
                  {row.data}
                </TableCell>
              </TableRow>
            ))}

            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
                colSpan={12}
                count={filter ? filter.length : rows.length}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  inputProps: { 'aria-label': 'rows per page' },
                  native: true,
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
                ActionsComponent={TablePaginationActions}
              />
            </TableRow>
          </TableFooter>
        </Table>
      </TableContainer>
    </Container>
  );
}

import { Link } from "./styles";
import { useHistory } from "react-router-dom";
import { useUser } from "../../providers/User"

const LinkGoTo = ({ children, to, isRegister }) => {

  const { setLoading } = useUser()

  const history = useHistory();

  const handleGoTo = (to) => {

    if(to === "/listagem-de-pacientes"){
      setLoading(true)
    }

    history.push({pathname: to, state: isRegister && true })
  };

  return <Link 
  onClick={() => handleGoTo(to)}>{children}</Link>;
};

export default LinkGoTo;
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { Button, TextField, Select, InputLabel, FormControl, FormHelperText } from '@material-ui/core'
import { useState } from 'react'
import './style.css'
import { makeStyles } from '@material-ui/core/styles';
import { useData } from '../../providers/IBGEData'
import { useUser } from '../../providers/User'
import { codGroups, nameGroup } from '../../services/groups'
import { codCategory, nameCategory } from '../../services/category'
import { raceColor } from '../../services/raceColor'
import { listVaccines, vaccines  } from '../../services/vaccine'
import { manufacturers } from '../../services/manufacturers'
import { v4 as uuidv4 } from 'uuid';
import { Container } from "./style"


const useStyles = makeStyles({
    outlined: {
        zIndex: 1,
        transform: 'translate(14px, 10px) scale(0.75) !important',
        pointerEvents: 'none',
        background: 'white',
        padding: '0 5px'
    },
    shrink: {
        zIndex: 1,
        transform: 'translate(14px, 10px) scale(0.75) !important',
        backgroundColor: 'transparent'
    },
    error: {
        color: 'red !important'
    },
})

const FormPatient = () => {
    const { 
        estados, municipiosPaciente, getMunicipiosPaciente, handleCodMunPac, 
        handleCodMunEst, codMunEst, codMunPac, getMunicipiosEstabelecimento,
        municipiosEstabelecimento
    } = useData()

    const { postPatient, setLoading } = useUser()

    const [sexo, setSexo] = useState()
    const [raca, setRaca] = useState()
    const [estadoPaciente, setEstadoPaciente] = useState()
    const classes = useStyles()
    // const history = useHistory()
    const [ estadoEstabelecimento, setEstadoEstabelecimento ] = useState('') 
    const [ municipioEstabelecimento, setMunicipioEstabelecimento ] = useState('') 
    const [ municipioPaciente, setMunicipioPaciente ] = useState('') 
    const [ grupoAtendimento, setGrupoAtendimento ] = useState('') 
    const [ categoriaAtendimento, setCategoriaAtendimento ] = useState('') 
    const [ fabricante, setFabricante ] = useState('') 
    const [ dose, setDose ] = useState('') 
    const [ vacinaNome, setVacinaNome ] = useState('')

    
    const schema = yup.object().shape({
        paciente_idade: yup.number().integer('digite um número inteiro').positive('digite um número positivo').required('Campo obrigatório!'),
        paciente_datanascimento: yup.date('data inválida!').required('campo obrigatório!'),
        paciente_enumsexobiologico: yup.string().required('campo obrigatório!'),
        paciente_racacor_valor: yup.string().required('campo obrigatório!'),
        paciente_endereco_uf: yup.string().required('campo obrigatório!'),
        paciente_endereco_nmmunicipio: yup.string().required('campo obrigatório!'),
        paciente_endereco_cep: yup.string().required('campo obrigatório!').matches(/^\d{5}\d{3}$|^\d{8}$|^\d{5}$/,'apenas 8 ou 5 números!'),
        estabelecimento_valor: yup.string().required().matches(/^\d{7}$/, 'Código de 7 números'),
        estabelecimento_razaosocial: yup.string().required('campo obrigatório!'),
        estalecimento_nofantasia: yup.string().required('campo obrigatório!'),
        estabelecimento_municipio_nome: yup.string().required('campo obrigatório!'),
        estabelecimento_uf: yup.string().required('campo obrigatório!'),
        vacina_grupoatendimento_nome: yup.string().required('campo obrigatório!'),
        vacina_categoria_nome: yup.string().required('campo obrigatório!'),
        vacina_lote: yup.string().max(30,'Máximo de 30 caracteres').required('campo obrigatório!'),
        vacina_fabricante_nome: yup.string().required('campo obrigatório!'),
        vacina_dataaplicacao: yup.date('data inválida!').required('campo obrigatório!'),
        vacina_descricao_dose: yup.string().required('campo obrigatório!'),
        vacina_nome: yup.string().required('campo obrigatório!')
    })

    const handleEstado = (event,bool) => {
        if(bool) {
            setEstadoPaciente(event)
            getMunicipiosPaciente(event)
        }
        else {
            setEstadoEstabelecimento(event)
            getMunicipiosEstabelecimento(event)
        }
    }

    const handleMunicipio = (event, bool) => {
        if(bool){
            setMunicipioPaciente(event)
            handleCodMunPac(event)
        }
        else {
            setMunicipioEstabelecimento(event)
            handleCodMunEst(event)
        }
    }
    
    const { register, handleSubmit, formState: { errors }, reset} = useForm({
        resolver:yupResolver(schema)
    })

    const handleForm = data => {
        data["document_id"] = uuidv4();
        data["paciente_id"] = uuidv4();
        data["paciente_racacor_codigo"] = raceColor[raca];
        data["paciente_endereco_coibgemunicipio"] = codMunPac;
        data["estabelecimento_municipio_codigo"] = codMunEst;
        data["vacina_grupoatendimento_codigo"] = codGroups[grupoAtendimento];
        data["vacina_categoria_codigo"] = codCategory[categoriaAtendimento];
        data["vacina_codigo"] = vaccines[vacinaNome];
        data["paciente_datanascimento"] = data["paciente_datanascimento"].toLocaleDateString();
        data["vacina_dataaplicacao"] = data["vacina_dataaplicacao"].toLocaleDateString()
        reset()
        setLoading(true)
        postPatient(data, "true")
    }

    return (
        <Container>

            <form onSubmit = {handleSubmit(handleForm)}>
                <div>
                    <TextField 
                        margin = 'normal'
                        variant = 'outlined'
                        label = 'Idade'
                        name = 'paciente_idade'
                        size = 'small'
                        color = 'primary'
                        type = 'number'
                        InputLabelProps={{ shrink: true, }}
                        required
                        inputProps = {register("paciente_idade")}
                        error = {!!errors.paciente_idade}
                        helperText = {errors.paciente_idade?.message}
                    />
                </div>
                <div>
                    <TextField 
                        classes = {{ input: classes.input}}
                        margin = 'normal'
                        variant = 'outlined'
                        label = 'Data de nascimento'
                        InputLabelProps={{ shrink: true, }}
                        name = 'paciente_datanascimento'
                        size = 'small'
                        color = 'primary'
                        required
                        type = 'date'
                        inputProps = {register("paciente_datanascimento")}
                        error = {!!errors.paciente_datanascimento}
                        helperText = {errors.paciente_datanascimento?.message}
                    />
                </div>
                <div>
                    <FormControl className='ola' variant="outlined" >
                        <InputLabel 
                            classes={{ outlined: classes.outlined, shrink: classes.shrink, error: classes.error }} 
                            htmlFor="demo-customized-select-native"
                            >
                                Sexo *
                            </InputLabel>
                        <Select
                            required
                            className="select" 
                            name = 'paciente_enumsexobiologico'
                            native
                            label = 'paciente_enumsexobiologico'
                            value={sexo}
                            onChange={(e)=>setSexo(e.target.value)}
                            inputProps = {register('paciente_enumsexobiologico')}
                            error = {!!errors.paciente_enumsexobiologico}
                            >
                            <option aria-label="tipo" value="" placeholder='tipo' />
                            <option value={'M'}>Masculino</option>
                            <option value={'F'}>Feminino</option>
                        </Select>
                        <FormHelperText style={{color:'#F34636', transform: 'translateY(-15px)'}}>{errors.paciente_enumsexobiologico?.message}</FormHelperText>
                    </FormControl>
                </div>
                <div>
                    <FormControl variant="outlined" >
                        <InputLabel 
                            classes={{ outlined: classes.outlined, shrink: classes.shrink, error: classes.error }} 
                            htmlFor="demo-customized-select-native"
                            >
                                Raça *
                            </InputLabel>
                        <Select
                            required
                            className="select" 
                            name = 'paciente_racacor_valor'
                            native
                            label = 'paciente_racacor_valor'
                            value={raca}
                            onChange={(e)=>setRaca(e.target.value)}
                            inputProps = {register('paciente_racacor_valor')}
                            error = {!!errors.paciente_racacor_valor}
                            >
                            <option aria-label="tipo" value="" placeholder='tipo' />
                            <option value={'AMARELA'}>Amarela</option>
                            <option value={'BRANCA'}>Branca</option>
                            <option value={'INDIGENA'}>Indígena</option>
                            <option value={'PRETA'}>Preta</option>
                            <option value={'PARDA'}>Parda</option>
                            <option value={'SEM INFORMACAO'}>Sem informação</option>
                        </Select>
                        <FormHelperText style={{color:'#F34636', transform: 'translateY(-15px)'}}>{errors.paciente_racacor_valor?.message}</FormHelperText>
                    </FormControl>
                </div>
                <div>
                    <FormControl variant="outlined" >
                        <InputLabel 
                            classes={{ outlined: classes.outlined, shrink: classes.shrink, error: classes.error }} 
                            htmlFor="demo-customized-select-native"
                            >
                                Estado *
                            </InputLabel>
                        <Select
                            required
                            className="select" 
                            name = 'paciente_endereco_uf'
                            native
                            label = 'paciente_endereco_uf'
                            value={estadoPaciente}
                            onChange={(e)=>handleEstado(e.target.value, true)}
                            inputProps = {register('paciente_endereco_uf')}
                            error = {!!errors.paciente_racacor_valor}
                            >
                            <option aria-label="tipo" value="" placeholder='tipo' />
                            { estados.map((e,ind)=><option key={ind} value={e.sigla}>{e.nome}</option>)}
                        </Select>
                        <FormHelperText style={{color:'#F34636', transform: 'translateY(-15px)'}}>{errors.paciente_endereco_uf?.message}</FormHelperText>
                    </FormControl>
                </div>
                <div>
                    <FormControl variant="outlined" >
                        <InputLabel 
                            classes={{ outlined: classes.outlined, shrink: classes.shrink, error: classes.error }} 
                            htmlFor="demo-customized-select-native"
                            >
                                Município de origem *
                            </InputLabel>
                        <Select
                            required
                            className="select" 
                            name = 'paciente_endereco_nmmunicipio'
                            native
                            label = 'paciente_endereco_nmmunicipio'
                            value={municipioPaciente}
                            onChange={(e)=>handleMunicipio(e.target.value, true)}
                            inputProps = {register('paciente_endereco_nmmunicipio')}
                            error = {!!errors.paciente_endereco_nmmunicipio}
                            >
                            <option aria-label="tipo" value="" placeholder='tipo' />
                            { municipiosPaciente.map((e,ind)=><option key={ind} value={e.nome}>{e.nome}</option>)}
                        </Select>
                        <FormHelperText style={{color:'#F34636', transform: 'translateY(-15px)'}}>{errors.paciente_endereco_nmmunicipio?.message}</FormHelperText>
                    </FormControl>
                </div>
                <div>
                    <TextField 
                        classes = {{ input: classes.input}}
                        margin = 'normal'
                        variant = 'outlined'
                        label = 'CEP'
                        InputLabelProps={{ shrink: true, }}
                        name = 'paciente_endereco_cep'
                        size = 'small'
                        color = 'primary'
                        required
                        inputProps = {register("paciente_endereco_cep")}
                        error = {!!errors.paciente_endereco_cep}
                        helperText = {errors.paciente_endereco_cep?.message}
                    />
                </div>
                <div>
                    <TextField 
                        classes = {{ input: classes.input}}
                        margin = 'normal'
                        variant = 'outlined'
                        label = 'Código CNES'
                        InputLabelProps={{ shrink: true, }}
                        name = 'estabelecimento_valor'
                        size = 'small'
                        color = 'primary'
                        required
                        type = 'number'
                        inputProps = {register("estabelecimento_valor")}
                        error = {!!errors.estabelecimento_valor}
                        helperText = {errors.estabelecimento_valor?.message}
                    />
                </div>
                <div>
                    <TextField 
                        classes = {{ input: classes.input}}
                        margin = 'normal'
                        variant = 'outlined'
                        label = 'Razão Social'
                        InputLabelProps={{ shrink: true, }}
                        name = 'estabelecimento_razaosocial'
                        size = 'small'
                        color = 'primary'
                        required
                        inputProps = {register("estabelecimento_razaosocial")}
                        error = {!!errors.estabelecimento_razaosocial}
                        helperText = {errors.estabelecimento_razaosocial?.message}
                    />
                </div>
                <div>
                    <TextField 
                        classes = {{ input: classes.input}}
                        margin = 'normal'
                        variant = 'outlined'
                        label = 'Nome Fantasia'
                        InputLabelProps={{ shrink: true, }}
                        name = 'estalecimento_nofantasia'
                        size = 'small'
                        color = 'primary'
                        required
                        inputProps = {register("estalecimento_nofantasia")}
                        error = {!!errors.estalecimento_nofantasia}
                        helperText = {errors.estalecimento_nofantasia?.message}
                    />
                </div>
                <div>
                    <FormControl variant="outlined" >
                        <InputLabel 
                            classes={{ outlined: classes.outlined, shrink: classes.shrink, error: classes.error }} 
                            htmlFor="demo-customized-select-native"
                            >
                                Estado do estabelecimento *
                            </InputLabel>
                        <Select
                            required
                            className="select" 
                            name = 'estabelecimento_uf'
                            native
                            label = 'estabelecimento_uf'
                            value={estadoEstabelecimento}
                            onChange={(e)=>handleEstado(e.target.value,false)}
                            inputProps = {register('estabelecimento_uf')}
                            error = {!!errors.estabelecimento_uf}
                            >
                            <option aria-label="tipo" value="" placeholder='tipo' />
                            { estados.map((e,ind)=><option key={ind} value={e.sigla}>{e.nome}</option>)}
                        </Select>
                        <FormHelperText style={{color:'#F34636', transform: 'translateY(-15px)'}}>{errors.estabelecimento_uf?.message}</FormHelperText>
                    </FormControl>
                </div>
                <div>
                    <FormControl variant="outlined" >
                        <InputLabel 
                            classes={{ outlined: classes.outlined, shrink: classes.shrink, error: classes.error }} 
                            htmlFor="demo-customized-select-native"
                            >
                                Município de estabelecimento *
                            </InputLabel>
                        <Select
                            required
                            className="select" 
                            name = 'estabelecimento_municipio_nome'
                            native
                            label = 'estabelecimento_municipio_nome'
                            value={municipioEstabelecimento}
                            onChange={(e)=>handleMunicipio(e.target.value,false)}
                            inputProps = {register('estabelecimento_municipio_nome')}
                            error = {!!errors.estabelecimento_municipio_nome}
                            >
                            <option aria-label="tipo" value="" placeholder='tipo' />
                            { municipiosEstabelecimento.map((e,ind)=><option key={ind} value={e.nome}>{e.nome}</option>)}
                        </Select>
                        <FormHelperText style={{color:'#F34636', transform: 'translateY(-15px)'}}>{errors.estabelecimento_municipio_nome?.message}</FormHelperText>
                    </FormControl>
                </div>
                <div>
                    <FormControl variant="outlined" >
                        <InputLabel 
                            classes={{ outlined: classes.outlined, shrink: classes.shrink, error: classes.error }} 
                            htmlFor="demo-customized-select-native"
                            >
                                Grupo atendimento *
                            </InputLabel>
                        <Select
                            required
                            className="select" 
                            name = 'vacina_grupoatendimento_nome'
                            native
                            label = 'vacina_grupoatendimento_nome'
                            value={grupoAtendimento}
                            onChange={(e)=>setGrupoAtendimento(e.target.value)}
                            inputProps = {register('vacina_grupoatendimento_nome')}
                            error = {!!errors.vacina_grupoatendimento_nome}
                            >
                            <option aria-label="tipo" value="" placeholder='tipo' />
                            { nameGroup.map((e,ind)=><option key={ind} value={e}>{e}</option>)}
                        </Select>
                        <FormHelperText style={{color:'#F34636', transform: 'translateY(-15px)'}}>{errors.vacina_grupoatendimento_nome?.message}</FormHelperText>
                    </FormControl>
                </div>
                <div>
                    <FormControl variant="outlined" >
                        <InputLabel 
                            classes={{ outlined: classes.outlined, shrink: classes.shrink, error: classes.error }} 
                            htmlFor="demo-customized-select-native"
                            >
                                Categoria atendimento *
                            </InputLabel>
                        <Select
                            required
                            className="select" 
                            name = 'vacina_categoria_nome'
                            native
                            label = 'vacina_categoria_nome'
                            value={categoriaAtendimento}
                            onChange={(e)=>setCategoriaAtendimento(e.target.value)}
                            inputProps = {register('vacina_categoria_nome')}
                            error = {!!errors.vacina_categoria_nome}
                            >
                            <option aria-label="tipo" value="" placeholder='tipo' />
                            { nameCategory.map((e,ind)=><option key={ind} value={e}>{e}</option>)}
                        </Select>
                        <FormHelperText style={{color:'#F34636', transform: 'translateY(-15px)'}}>{errors.vacina_categoria_nome?.message}</FormHelperText>
                    </FormControl>
                </div>
                <div>
                    <TextField 
                        margin = 'normal'
                        variant = 'outlined'
                        label = 'Lote da vacina'
                        name = 'vacina_lote'
                        size = 'small'
                        color = 'primary'
                        InputLabelProps={{ shrink: true, }}
                        required
                        inputProps = {register("vacina_lote")}
                        error = {!!errors.vacina_lote}
                        helperText = {errors.vacina_lote?.message}
                    />
                </div>
                <div>
                    <FormControl variant="outlined" >
                        <InputLabel 
                            classes={{ outlined: classes.outlined, shrink: classes.shrink, error: classes.error }} 
                            htmlFor="demo-customized-select-native"
                            >
                                Fabricante da vacina *
                            </InputLabel>
                        <Select
                            required
                            className="select" 
                            name = 'vacina_fabricante_nome'
                            native
                            label = 'vacina_fabricante_nome'
                            value={fabricante}
                            onChange={(e)=>setFabricante(e.target.value)}
                            inputProps = {register('vacina_fabricante_nome')}
                            error = {!!errors.vacina_fabricante_nome}
                            >
                            <option aria-label="tipo" value="" placeholder='tipo' />
                            { manufacturers.map((e,ind)=><option key={ind} value={e}>{e}</option>)}
                        </Select>
                        <FormHelperText style={{color:'#F34636', transform: 'translateY(-15px)'}}>{errors.vacina_fabricante_nome?.message}</FormHelperText>
                    </FormControl>
                </div>
                <div>
                    <TextField 
                        classes = {{ input: classes.input}}
                        margin = 'normal'
                        variant = 'outlined'
                        label = 'Data da aplicação'
                        InputLabelProps={{ shrink: true, }}
                        name = 'vacina_dataaplicacao'
                        size = 'small'
                        color = 'primary'
                        required
                        type = 'date'
                        inputProps = {register("vacina_dataaplicacao")}
                        error = {!!errors.vacina_dataaplicacao}
                        helperText = {errors.vacina_dataaplicacao?.message}
                    />
                </div>
                <div>
                    <FormControl variant="outlined" >
                        <InputLabel 
                            classes={{ outlined: classes.outlined, shrink: classes.shrink, error: classes.error }} 
                            htmlFor="demo-customized-select-native"
                            >
                                Qual dose? *
                            </InputLabel>
                        <Select
                            required
                            className="select" 
                            name = 'vacina_descricao_dose'
                            native
                            label = 'vacina_descricao_dose'
                            value={dose}
                            onChange={(e)=>setDose(e.target.value)}
                            inputProps = {register('vacina_descricao_dose')}
                            error = {!!errors.vacina_descricao_dose}
                            >
                            <option aria-label="tipo" value="" placeholder='tipo' />
                            <option value={'1ª dose'}>Primeira dose</option>
                            <option value={'2ª dose'}>Segunda dose</option>
                            <option value={'3ª dose'}>Terceira dose</option>
                        </Select>
                        <FormHelperText style={{color:'#F34636', transform: 'translateY(-15px)'}}>{errors.vacina_descricao_dose?.message}</FormHelperText>
                    </FormControl>
                </div>
                <div>
                    <FormControl variant="outlined" >
                        <InputLabel 
                            classes={{ outlined: classes.outlined, shrink: classes.shrink, error: classes.error }} 
                            htmlFor="demo-customized-select-native"
                            >
                                Qual vacina? *
                            </InputLabel>
                        <Select
                            required
                            className="select" 
                            name = 'vacina_nome'
                            native
                            label = 'vacina_nome'
                            value={vacinaNome}
                            onChange={(e)=>setVacinaNome(e.target.value)}
                            inputProps = {register('vacina_nome')}
                            error = {!!errors.vacina_nome}
                            >
                            <option aria-label="tipo" value="" placeholder='tipo' />
                            { listVaccines.map((e,ind)=><option key={ind} value={e}>{e}</option>)}
                        </Select>
                        <FormHelperText style={{color:'#F34636', transform: 'translateY(-15px)'}}>{errors.vacina_nome?.message}</FormHelperText>
                    </FormControl>
                </div>

                <div>
                    <Button type='submit' variant='contained' color='primary'>Cadastrar</Button>
                </div>
            </form>
        </Container>
    )
}

export default FormPatient

import styled from "styled-components";

export const Container = styled.div`
    form {
        width: 100vw;
        height: 100vh;
        max-width: 1100px;
        max-height: 500px;
        margin: 0 auto;
        display: flex;
        justify-content: space-space-between;
        flex-wrap: wrap;
        align-items: center;
        flex-direction: column;
        background-color: white;
        border-radius: 10px;
    }

    @media(max-width: 741px) {
        form {
            flex-wrap: nowrap;
            overflow: scroll;
        }
    }
`
export const Title = styled.h1`
    color: #232A34;
`
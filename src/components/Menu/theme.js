export const theme = {
    primaryDark: "#C0C0C0",
    primaryLight: "#0a090d",
    primaryHover: "#65AFFF",
    backgroundColor: "#1B2845",
    mobile: "576px",
  };
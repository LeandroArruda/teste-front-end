import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as yup from 'yup'
import { Button, TextField, Select, InputLabel, FormControl, FormHelperText } from '@material-ui/core'
import { useState } from 'react'
import './style.css'
import { makeStyles } from '@material-ui/core/styles';
import { Container, ButtonGroup } from "./style"
import { useUser } from '../../providers/User'

const useStyles = makeStyles({
    outlined: {
        zIndex: 1,
        transform: 'translate(14px, 29px) scale(1)',
        pointerEvents: 'none'
    },
    shrink: {
        zIndex: 1,
        transform: 'translate(14px, 10px) scale(0.75) !important'
    },
    error: {
        color: 'red !important'
    }
})

const FormRegister = ({ render, setRender }) => {

    const { users, updateUser, createUser, setLoading } = useUser()

    const [user, setUser] = useState(() => render ? users.find(e=>e.id === render) : false)

    const classes = useStyles()

    const [ tipo, setTipo ] = useState('') 
    const [ isHabilit, setIsHabilit ] = useState('')

    const schema = yup.object().shape({
        email: yup.string().email('Email inválido').required('campo obrigatório!'),
        name: yup.string().min(5).required('campo obrigatório!'),
        password: yup
            .string()
            .min(8, 'mínimo de 8 caracteres')
            .matches(
                /^((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
                'Senha deve conter letra mínuscula, maiúscula, número caractere!'
            ),
        isAdmin: yup.bool().required('campo obrigatório'),
        isEnabled: yup.bool().required("campo obrigatório")
    })

    const { register, handleSubmit, formState: { errors }, reset} = useForm({
        resolver:yupResolver(schema)
    })

    const handleForm = data => {
        setLoading(true)
        if(user && render){
            delete data.password
            updateUser(render,data)
            setRender(false)
        }else{
            createUser(data)
        }
        resetFields()
    }

    const resetFields = () => {
        if(render)
        setRender(false)
        setUser(false)
        reset()
    }

    return (
        <Container>
            <form onSubmit = {handleSubmit(handleForm)}>
                <div>
                    <TextField 
                        defaultValue = {user ? user.email : "" }
                        margin = 'normal'
                        variant = 'outlined'
                        label = 'Email'
                        name = 'email'
                        size = 'small'
                        color = 'primary'
                        inputProps = {register("email")}
                        error = {!!errors.email}
                        helperText = {errors.email?.message}
                    />
                </div>
                <div>
                    <TextField 
                        defaultValue = {user ? user.name : "" }
                        margin = 'normal'
                        variant = 'outlined'
                        label = 'Nome'
                        name = 'name'
                        size = 'small'
                        color = 'primary'
                        inputProps = {register('name')}
                        error = {!!errors.name}
                        helperText = {errors.name?.message}
                    />
                </div>
                <div>
                    <TextField 
                        disabled = {user ? true : false}
                        defaultValue = {user ? user.password : "" }
                        margin = 'normal'
                        variant = 'outlined'
                        label = 'Senha'
                        name = 'password'
                        size = 'small'
                        color = 'primary'
                        required
                        inputProps = {register('password')}
                        error = {!!errors.password}
                        helperText = {errors.password?.message}
                    />
                </div>
                <div>
                    <FormControl variant="outlined" >
                        <InputLabel 
                            classes={{ outlined: classes.outlined, shrink: classes.shrink, error: classes.error }} 
                            htmlFor="demo-customized-select-native"
                            >
                                Tipo
                            </InputLabel>
                        <Select
                            className="select" 
                            name = 'isAdmin'
                            native
                            label = 'isAdmin'
                            defaultValue={!user? tipo : user.isAdmin ? true : false }
                            onChange={(e)=>setTipo(e.target.value)}
                            inputProps = {register('isAdmin')}
                            error = {!!errors.type}
                            >
                            <option aria-label="tipo" value="" placeholder='tipo' />
                            <option value={true}>Admin</option>
                            <option value={false}>Comum</option>
                        </Select>
                        <FormHelperText style={{color:'#F34636', transform: 'translateY(-15px)'}}>{errors.isAdmin?.message}</FormHelperText>
                    </FormControl>
                </div>
                <div>
                    <FormControl variant="outlined" >
                        <InputLabel 
                            classes={{ outlined: classes.outlined, shrink: classes.shrink, error: classes.error }} 
                            htmlFor="demo-customized-select-native"
                            >
                                Habilitado?
                            </InputLabel>
                        <Select
                            className="select" 
                            name = 'isEnabled'
                            native
                            label = 'isEnabled'
                            defaultValue={ !user? isHabilit : user.isEnabled ? true : false }
                            onChange={ (e)=>setIsHabilit(e.target.value) }
                            inputProps = {register('isEnabled')}
                            error = {!!errors.isEnabled}
                            >
                            <option aria-label="tipo" value="" placeholder='tipo' />
                            <option value={true}>Sim</option>
                            <option value={false}>Não</option>
                        </Select>
                        <FormHelperText style={{color:'#F34636', transform: 'translateY(-15px)'}}>{errors.isEnabled?.message}</FormHelperText>
                    </FormControl>
                </div>
                <ButtonGroup>
                    <Button style={{ marginRight : "10px"}} type='submit' variant='contained' >Cadastrar</Button>
                    <Button type='reset' onClick={resetFields} variant='contained' color='primary'>Cancelar</Button>
                </ButtonGroup>
            </form>
        </Container>
    )
}

export default FormRegister

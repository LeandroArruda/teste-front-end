import styled from "styled-components";

export const Container = styled.div`
        width: 100%;
        height: 100%;
        max-width: 400px;
        max-height: 500px;
        margin: 0 auto;
        display: flex;
        justify-content: center;
        align-items: center;
        flex-direction: column;
        background-color: white;
        border-radius: 20px;

        @media(max-width: 400px) {
            form {
                width: 100%;
                height: 100%;
                max-width: 400px;
                max-height: 500px;
                margin: 0 auto;
                display: flex;
                justify-content: center;
                align-items: center;
                flex-direction: column;
                background-color: white;
                border-radius: 20px;
            }
        }
`

export const ButtonGroup  = styled.span`
    @media(max-width:400px){
        display: flex;
        flex-direction: row;
        flex-wrap: nowrap;
    }
`
export const Title = styled.h1`
    color: #232A34;
`
import { bool } from "prop-types";
import { StyledMenu } from "./styles";
import { motion } from 'framer-motion';
import LinkGoTo from "../LinkGoTo";
import { useUser } from "../../providers/User";

const MenuItem = ({ open, menuLink, isNew }) => {
  const { isAdmin, setLoading } = useUser()
  const token = JSON.parse(localStorage.getItem("token"));
  const outherKey = menuLink.length;

  const handleLogOut = () => {
    localStorage.clear();
    setLoading(true)
  };

  return (
    <StyledMenu open={open}>
      {!!menuLink &&
        menuLink.map(({ content, to, anchor }, index) => {
            return (
              <motion.span
              key={index}
              whileHover={{ 
               scale: 1.1,
               y: -2
              }}
              >
                { content === 'Administrador' && isAdmin ? (
                  <LinkGoTo to={to} >
                    {content}
                  </LinkGoTo>
                ) : content !== 'Administrador' ? (
                  <LinkGoTo to={to} >
                    {content}
                  </LinkGoTo>
                ) : null}
              </motion.span>
            );
          }
        )}
      {token && (
              <motion.span
              whileHover={{ 
               scale: 1.1,
               y: -2
              }}
              >
                <a key={outherKey} onClick={handleLogOut} href="/entrar">
                  LogOut
                </a>
              </motion.span>
      )}
    </StyledMenu>
  );
};
MenuItem.propTypes = {
  open: bool.isRequired,
};
export default MenuItem;
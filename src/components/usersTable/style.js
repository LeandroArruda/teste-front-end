import styled from "styled-components";

export const Container = styled.div`
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    width: 98vw;
    border-radius: 10px;
 
    div{
        margin: 5px;
    }

    button {
        cursor: pointer;
        background-color: transparent;
    }
`
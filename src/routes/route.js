import { Route as ReactDOMRoute, Redirect } from "react-router-dom";
import { useUser } from "../providers/User";


const Route = ({component: Component, isAuth }) => {

  const { users } = useUser()

  const token = JSON.parse(localStorage.getItem("token") || null)

  return (
      <ReactDOMRoute
        render={() => !!isAuth === !!token ? 
        <Component /> : !!users ? <Redirect to="/listagem-de-pacientes" /> : 
        <Redirect to="/entrar" /> }
      />
    );
};

export default Route;
import { Switch, useLocation } from "react-router-dom";
import AdminPage from "../pages/AdminPage";
import LoginPage from "../pages/LoginPage";
import PreviewPage from "../pages/PreviewPage";
import RecordsPage from "../pages/RecordsPage";
import Route from "./route";

const Routes = () => {

  const location = useLocation()
  return (
    <Switch location={location} key={location.pathname} >
        <Route path="/entrar" component={LoginPage} />
        <Route exact path="/" component={LoginPage} />
        <Route path="/pagina-do-admin" component={AdminPage} isAuth />
        <Route path="/listagem-de-pacientes" component={PreviewPage} isAuth />
        <Route path="/cadastro-de-pacientes" component={RecordsPage} isAuth />
    </Switch>
  );
};

export default Routes;
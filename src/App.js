import Routes from "./routes";
import Backdrop from "./components/Backdrop"
import { useUser } from "./providers/User"

function App() {
  const { loading } = useUser()
  return (
        <>
          {loading && <Backdrop /> }
          <Routes></Routes>
        </>
  )     
}

export default App;

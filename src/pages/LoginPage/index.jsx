import FormLogin from "../../components/FormLogin"
import { Container } from "./style"
import Notification from "../../components/Notification"

const LoginPage = () =>{
    return (
        <Container>
            <FormLogin />
            <Notification />
        </Container>
    )
}

export default LoginPage;
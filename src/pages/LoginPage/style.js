import styled, { keyframes } from "styled-components";


const transOpacity = keyframes`
    from{
        opacity:0;
    }
    to {
        opacity: 1;
    }
`

export const Container = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    background-image: url('images/vacina.jpg');
    background-size: cover;
    background-blend-mode: lighten;
    animation: ${transOpacity} 1s linear;
    /* filter: brightness(.5); */
`
export const Title = styled.h1`
    color: #232A34;
`
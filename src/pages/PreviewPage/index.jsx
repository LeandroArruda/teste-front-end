import PatientsTable from "../../components/PatientsTable"
import Menu from "../../components/Menu";
import { menuLinkPreviews } from "../../services/menuData"
import PatientsMap from "../../components/PatientsMap";
import { Container, Buttons } from "./style"
import { makeStyles } from '@material-ui/core/styles';
import { useState } from "react";
import { useUser } from "../../providers/User"
import Notification from "../../components/Notification"

const useStyles = makeStyles((theme) => ({
    root: {
      '& > *': {
        margin: "10px",
      },
    },
  }));

const PreviewPage = () =>{

    const { setLoading } = useUser()

    const callList = () => {
        setLoading(true)
        setTimeout(() => {
            setRender(true)
        }, 200);
    }

    const classes = useStyles();

    const [render, setRender] = useState(true)

    return (
        <>
            <Menu menuLink={menuLinkPreviews} />
            <Container>
                <div className={classes.root}>
                    <Buttons 
                        variant="outlined"
                        onClick={()=>setRender(false)}
                        
                    > 
                        Ver Lista
                    </Buttons>

                    <Buttons 
                        variant="outlined" 
                        color="primary"
                        onClick={callList}
                    >
                        Ver Mapa
                    </Buttons>

                </div>
                
                { render ? <PatientsMap /> : <PatientsTable /> }
                
                <Notification />
                
            </Container>
        </>
    )
}

export default PreviewPage;
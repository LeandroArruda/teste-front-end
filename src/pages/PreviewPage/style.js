import styled, { keyframes } from "styled-components";
import { Button } from "@material-ui/core";

const transOpacity = keyframes`
    from{
        opacity:0;
    }
    to {
        opacity: 1;
    }
`

export const Container = styled.div`
    width: 99vw;
    height: 98vh; 
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-direction: column;
    background-image: url('images/vacina.jpg');
    background-size: cover;
    background-blend-mode: lighten;
    animation: ${transOpacity} 2s linear;
`
export const Buttons = styled(Button)`
    margin-top: 30px;
`
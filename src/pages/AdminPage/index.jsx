import Menu from "../../components/Menu";
import { menuLinkAdmin } from "../../services/menuData"
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import { useState } from "react";
import FormRegister from "../../components/FormRegistration"
import UsersTable from "../../components/usersTable"
import Notification from "../../components/Notification";
import { Container } from "./styles"

const useStyles = makeStyles((theme) => ({
    root: {
      '& > *': {
        margin: theme.spacing(1),
      },
    },
  }));

const AdminPage = () =>{

    const classes = useStyles();

    const [render, setRender] = useState(false)


    return (
        <>  
            <Menu menuLink={menuLinkAdmin} />
            <Container>
                <span className={classes.root}>
                    <Button 
                        variant="outlined"
                        onClick={()=>setRender(false)}
                    > 
                        Lista de Usuários
                    </Button>

                    <Button 
                        variant="outlined" 
                        onClick={()=>setRender(true)}
                    >
                        Novo usuário
                    </Button>

                </span>
                
                { render ? <FormRegister render={render} setRender={setRender} /> : <UsersTable setRender={setRender} /> }
                <Notification />
            </Container>
        </>
    )
}

export default AdminPage;
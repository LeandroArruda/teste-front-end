import styled, { keyframes } from "styled-components";

const transOpacity = keyframes`
    from{
        opacity:0;
    }
    to {
        opacity: 1;
    }
`

export const Container = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;
    justify-content: space-around;
    align-items: center;
    flex-direction: column;
    background-image: url('images/vacina.jpg');
    background-size: cover;
    background-blend-mode: lighten;
    animation: ${transOpacity} 1s linear;

    @media(max-width: 516px) {
        button{
            width: 180px;
            height: 40px;
            text-align: center;
        }
        span {
            width: 180px;
            display: flex;
            flex-direction: column;
            align-items: end;
        }
    }
`
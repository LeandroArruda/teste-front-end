import FormPatient from "../../components/FormPatient"
import Menu from "../../components/Menu";
import { menuLinkRecords } from "../../services/menuData"
import { Container } from "./style"


const RecordsPage = () =>{
    return (
        <>
            <Menu menuLink={menuLinkRecords} />
            <Container>
                <FormPatient />
            </Container>
        </>
    )
}

export default RecordsPage;
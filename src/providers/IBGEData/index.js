import { createContext, useContext, useEffect, useState } from "react";
import apiIBGE from "../../services/apiIBGE";

const UserContext = createContext();

export const DataProvider = ({ children }) => {
    const [estados, setEstados] = useState([]);
    const [municipiosPaciente, setMunicipiosPaciente] = useState([]);
    const [municipiosEstabelecimento, setMunicipiosEstabelecimento] = useState([]);
    const [codMunPac, setCodMunPac] = useState([]);
    const [codMunEst, setCodMunEst] = useState([]);

  useEffect(()=>{
      apiIBGE
        .get("/estados")
        .then((response) => {
            setEstados(response.data)
        })
        .catch((e) => {
          console.log(e);
        });
  },[])

  const handleCodMunPac = (municipio) => {
    const m = municipiosPaciente.find(e=>e.nome === municipio)
    setCodMunPac(m.id)
  }

  const handleCodMunEst = (municipio) => {
    const m = municipiosEstabelecimento.find(e=>e.nome === municipio)
    setCodMunEst(m.id)
  }

  const getMunicipiosPaciente = (estado) => {
    apiIBGE
      .get(`/estados/${estado.toLowerCase()}/municipios`)
      .then(response=>{setMunicipiosPaciente(response.data)})
      .catch(e=>console.log(e))
  }

  const getMunicipiosEstabelecimento = (estado) => {
      apiIBGE
        .get(`/estados/${estado.toLowerCase()}/municipios`)
        .then(response=>{setMunicipiosEstabelecimento(response.data)})
        .catch(e=>console.log(e))
  }

  return (
      <UserContext.Provider value={{ 
        estados, municipiosPaciente, getMunicipiosPaciente, handleCodMunPac, 
        handleCodMunEst, codMunEst, codMunPac, getMunicipiosEstabelecimento,
        municipiosEstabelecimento
      }}>
        {children}
      </UserContext.Provider>
    );    
};

export const useData = () => useContext(UserContext);
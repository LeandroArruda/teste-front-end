import { createContext, useContext, useEffect, useState } from "react";
import api from "../../services/api";
import { useHistory } from "react-router-dom";
import jwt_decode from "jwt-decode";
import {
  notifyErrorLogin, notifyRegisterSuccess,
  notifyDeleted, existingEmail, notifyRegisterUpdate,
  notifyErrorUser, notifyLoginSucces, userDisabled  
} from "../../services/notifyData"

const UserContext = createContext();

export const UserProvider = ({ children }) => {
  const history = useHistory()
  const [patientsList, setPatientsList] = useState([]);
  const [token, setToken] = useState(JSON.parse(localStorage.getItem('token')) || null);
  const [id, setId] = useState(JSON.parse(localStorage.getItem('id')) || null);
  const [isAdmin, setIsAdmin] = useState(false);
  const [municipiosSC, setMunicipiosSC] = useState([]);
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(()=>{
    if (!!id && !!token) {
      getPatients(token)
      getOneUser(id,token)
      getMunSC(token)
      getUsers(token)
    }
  },[])

  const login = (data) =>{
    api
      .post("/login", data)
      .then((response) => {
        const { sub } = jwt_decode(response.data.accessToken);
        setToken(response.data.accessToken);
        setId(sub);
        localStorage.setItem("token", JSON.stringify(response.data.accessToken));
        localStorage.setItem("id", JSON.stringify(sub));
        getOneUser(sub, response.data.accessToken, login)
        getPatients(response.data.accessToken)
        getMunSC(response.data.accessToken)
        getUsers(response.data.accessToken)
        // history.push("/listagem-de-pacientes")
      })
      .catch((e) => {
        setLoading(false)
        notifyErrorLogin()
      });
  }

  const getPatients = (token, isRegister) => {
    api
      .get("/pacientes", { headers: {
        'Authorization': `Bearer ${token}` 
      }})
      .then(response=>{
        setPatientsList(response.data)
        if(isRegister) {
          history.push("/listagem-de-pacientes")
          setLoading(false)
          setTimeout(() => {
            notifyRegisterSuccess()
          }, 200);
        }
      })
      .catch((e) => {
        localStorage.clear()
        history.push("/entrar")
      });
  }

  const getOneUser = (id, token, login) =>{
    api
    .get(`/users/${id}`, { headers: {
      'Authorization': `Bearer ${token}` 
    }})
    .then(response=>{
      setIsAdmin(response.data.isAdmin)
      if(!response.data.isEnabled){
        localStorage.clear()
        history.push("/entrar")
        setTimeout(() => {
          userDisabled()
        }, 1000);
      }else{
        if(login){
          setTimeout(() => {
            notifyLoginSucces(response.data.name.split(' ')[0])
          }, 1000);
        }
      }
    })
    .catch((e) => {
      console.log(e);
    });
  }

  const getUsers = (token) =>{
    api
    .get(`/users`, { headers: {
      'Authorization': `Bearer ${token}` 
    }})
    .then(response=>{
      setUsers(response.data)
      setTimeout(() => {
        setLoading(false)
      }, 2000);
    })
    .catch((e) => {
      console.log(e);
      setLoading(false)
    });
  }

  const getMunSC = (token) => {
    api
    .get(`/municipios`, { headers: {
      'Authorization': `Bearer ${token}` 
    }})
    .then(response=>{
      setMunicipiosSC(response.data)
    })
    .catch((e) => {
      console.log(e)
    });
  }

  const postPatient = (data, isRegister) => {
    api
    .post(`/pacientes`, data, { headers: {
      'Authorization': `Bearer ${token}` 
    }})
    .then(response=>{
      getPatients(token, isRegister)
    })
    .catch((e) => {
      console.log(e);
    });
  }

  const updateUser = (id, data) => {
    api
    .patch(`/users/${id}`, data, { headers: {
      'Authorization': `Bearer ${token}` 
    }})
    .then(response=>{
      getUsers(token)
      setTimeout(() => {
        notifyRegisterUpdate()
      }, 1000);
    })
    .catch((e) => {
      setLoading(false)
      setTimeout(() => {
        notifyErrorUser()
      }, 1000);
    });
  }

  const createUser = (data) => {
    api
    .post(`/register`, data)
    .then(response=>{
      notifyRegisterSuccess()
      getUsers(token)
    })
    .catch((e) => {
      existingEmail()
      setTimeout(() => {
        setLoading(false)
      }, 500);
    });
  }

  const deleteUser = (id) => {
    api
    .delete(`/users/${id}`, { headers: {
      'Authorization': `Bearer ${token}`
    }})
    .then(response=>{
      getUsers(token)
      setTimeout(() => {
        notifyDeleted()
      }, 1000);
    })
    .catch((e) => {
      console.log(e);
    });
  }

  return (
      <UserContext.Provider value={{ 
        patientsList, getPatients, login, id, isAdmin, municipiosSC,
        postPatient, users, updateUser, createUser, loading, setLoading,
        deleteUser
      }}>
        {children}
      </UserContext.Provider>
    );    
};

export const useUser = () => useContext(UserContext);
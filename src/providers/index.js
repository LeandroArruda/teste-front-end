import { UserProvider } from "./User";
import { DataProvider } from './IBGEData'

const Providers = ({ children }) => {
  return (
    <UserProvider>
      <DataProvider>
        {children}
      </DataProvider>
    </UserProvider>
  );
};

export default Providers;
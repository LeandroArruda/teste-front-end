import axios from "axios";

const api = axios.create({
  baseURL: "https://app-teste-front-8.herokuapp.com/",
});

export default api;

export const menuLinkPreviews = [
  {
    anchor: false,
    to: "/cadastro-de-pacientes",
    content: "Cadastrar paciente",
  },
  {
    anchor: false,
    to: "/pagina-do-admin",
    content: "Administrador",
  }
];

export const menuLinkAdmin = [
  {
    anchor: false,
    to: "/listagem-de-pacientes",
    content: "Lista de pacientes"
  },
  {
    anchor: false,
    to: "/cadastro-de-pacientes",
    content: "Cadastrar paciente"
  }
];

export const menuLinkRecords = [
  {
    anchor: false,
    to: "/listagem-de-pacientes",
    content: "Lista de pacientes"
  },
  {
    anchor: false,
    to: "/pagina-do-admin",
    content: "Administrador",
  }
];

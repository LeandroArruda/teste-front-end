export const codCategory = {
    "Faixa Etaria": "2",
    "Trabalhadores de Saúde": "9",
    "Povos e Comunidades Tradicionais": "6",
    "Pessoas de 60 anos ou mais institucionalizadas": "3",
    "Povos Indígenas": "7",
    "Comorbidades": "1",
    "Forças de Segurança e Salvamento": "5",
    "Trabalhadores da Educação": "8"
}

export const nameCategory = [
    "Comorbidades",
    "Faixa Etaria",
    "Forças de Segurança e Salvamento",
    "Pessoas de 60 anos ou mais institucionalizadas",
    "Povos Indígenas",
    "Povos e Comunidades Tradicionais",
    "Trabalhadores da Educação",
    "Trabalhadores de Saúde"
]
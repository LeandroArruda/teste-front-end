import { toast } from "react-toastify";
/* 
opções
toast.success
toast.warn
toast.error
toast.info
*/
export const notifyError = (error) =>
  toast.error(error, {
    autoClose: 2000,
    hideProgressBar: true,
    position: "top-center",
  });

export const notifyErrorClient = () =>
  toast.error("Erro inesperado na atualização do cadastro do cliente.", {
    autoClose: 2000,
    hideProgressBar: true,
    position: "top-center",
  });

export const notifyErrorUser = () =>
  toast.error("Erro inesperado na atualização!", {
    autoClose: 2000,
    hideProgressBar: true,
    position: "top-center",
  });

export const notifyErrorEmployee = () =>
  toast.error("Erro inesperado na gravação do funcionário.", {
    autoClose: 2000,
    hideProgressBar: true,
    position: "top-center",
  });

export const notifyErrorLogin = () =>
  toast.error("Usario ou senha incorretos!", {
    autoClose: 2000,
    hideProgressBar: true,
    position: "top-center",
  });

  export const existingEmail = () =>
  toast.error("Email já cadastrado", {
    autoClose: 2000,
    hideProgressBar: true,
    position: "top-center",
  });

export const userDisabled = () =>
toast.error("Usuário desabilitado!", {
  autoClose: 2000,
  hideProgressBar: true,
  position: "top-center",
});

export const notifyRegisterSuccess = () =>
toast.success("Cadastrado com sucesso!", {
  autoClose: 2000,
  hideProgressBar: true,
  position: "top-center",
});

export const notifyLoginSucces= (name) =>
toast.success(`Bem vindo ${name}!!!`, {
  autoClose: 2000,
  hideProgressBar: true,
  position: "top-center",
});

export const notifyRegisterUpdate = () =>
  toast.success("Usuário Atualizado com sucesso!", {
    autoClose: 2000,
    hideProgressBar: true,
    position: "top-center",
});

export const notifyDeleted = () =>
  toast.success("Excluído com sucesso!", {
    autoClose: 2000,
    hideProgressBar: true,
    position: "top-center",
  });
